Scald IPTC data
===============

The Scald IPTC data module can provide default information for scald image descriptions, copyrights, authors and other data.
This information is extracted from the image IPTC metadatas.

It's a fork of an existing sandbox : https://www.drupal.org/sandbox/gifad/2029915