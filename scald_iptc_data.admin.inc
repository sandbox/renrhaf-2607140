<?php
/**
 * @file
 * Functionalities to administer the Scald IPTC data.
 */

/**
 * Provides the form for administering scald IPTC data.
 */
function scald_iptc_data_admin_settings_form() {
  $form['scald_iptc_data_auto_fill'] = array(
    '#type' => 'radios',
    '#title' => t('IPTC data extracting mode'),
    '#description' => t('Define if the IPTC information should be automatically added to the fields or not.'),
    '#default_value' => variable_get('scald_iptc_data_auto_fill', SCALD_IPTC_DATA_AUTO_FILL),
	'#options' => array(SCALD_IPTC_DATA_AUTO_FILL => t('Automatic filling of fields.'), SCALD_IPTC_DATA_MANUAL_FILL => t('Manual filling of fields.'))
  );

  return system_settings_form($form);
}
