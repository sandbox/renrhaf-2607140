/**
 * @file
 * Copy IPTC data to field behavior
 */
(function ($) {
    /**
     * Copy IPTC data to the linked field
     */
    Drupal.behaviors.scald_iptc_copy = {
        attach: function (context) {
            $('span.copy-iptc-value').click(function(event) {
                var value = $(this).parent('.description').find('.iptc-value').html();
                var field = $(this).parent('.description').parent().find('input, textarea');

                // Set the field value
                $(field).val(value);

                // Check if CKEditor is present
                if ($(this).parents('.form-wrapper').find('.cke').length > 0) {
                    var id = $(field).attr('id');
                    CKEDITOR.instances[id].setData(value);
                }
            });
        }
    };
})(jQuery);
