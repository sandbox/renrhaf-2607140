<?php

/**
 * @file
 * IPTC Tags importer for Scald IPTC data.
 */

/**
 * Analyses an image and extract its IPTC metadata.
 *
 * @see http://php.net/iptcparse
 * @see http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/IPTC.html
 *
 * @param $uri string the URI of the image file
 *
 * @return array
 */
function scald_iptc_data_analyze($uri) {
  $info = array();
  $image_info = array();
  getimagesize(drupal_realpath($uri), $image_info);

  if (isset($image_info['APP13'])) {
    $IPTC_parsed = iptcparse($image_info['APP13']);
    if (is_array($IPTC_parsed)) {
      foreach ($IPTC_parsed as $IPTC_key_raw => $IPTC_values) {
        list($IPTC_record, $IPTC_tag_key) = explode('#', $IPTC_key_raw);
        $IPTC_tag_key = intval(ltrim($IPTC_tag_key, '0'));
        foreach ($IPTC_values as $key => $value) {
          $IPTC_record_name = _scald_iptc_IPTC_record_name($IPTC_record);
          $IPTC_record_tag_name = _scald_iptc_IPTC_record_tag_name($IPTC_record, $IPTC_tag_key);
          if (isset($info['iptc'][$IPTC_record_name][$IPTC_record_tag_name])) {
            $info['iptc'][$IPTC_record_name][$IPTC_record_tag_name][] = $value;
          }
          else {
            $info['iptc'][$IPTC_record_name][$IPTC_record_tag_name] = array($value);
          }
        }
      }
    }
  }

  return $info;
}

/**
 * Returns the IPTC record name given a string.
 *
 * @see http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/IPTC.html
 *
 * @param $IPTC_record string
 *
 * @return string
 */
function _scald_iptc_IPTC_record_name($IPTC_record) {
  static $IPTC_record_name = array();
  if (empty($IPTC_record_name)) {
    $IPTC_record_name = array(
      1 => 'IPTCEnvelope',
      2 => 'IPTCApplication',
      3 => 'IPTCNewsPhoto',
      7 => 'IPTCPreObjectData',
      8 => 'IPTCObjectData',
      9 => 'IPTCPostObjectData',
    );
  }
  return (isset($IPTC_record_name[$IPTC_record]) ? $IPTC_record_name[$IPTC_record] : '');
}

/**
 * Returns the IPTC tag name.
 *
 * @see http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/IPTC.html
 *
 * @param $IPTC_record int
 * @param $IPTC_tag_key int
 *
 * @return string
 */
function _scald_iptc_IPTC_record_tag_name($IPTC_record, $IPTC_tag_key) {
  static $IPTC_record_tag_name = array();
  if (empty($IPTC_record_tag_name)) {
    $IPTC_record_tag_name = array(
      1 => array( // IPTC EnvelopeRecord Tags
        0 => 'EnvelopeRecordVersion',
        5 => 'Destination',
        20 => 'FileFormat',
        22 => 'FileVersion',
        30 => 'ServiceIdentifier',
        40 => 'EnvelopeNumber',
        50 => 'ProductID',
        60 => 'EnvelopePriority',
        70 => 'DateSent',
        80 => 'TimeSent',
        90 => 'CodedCharacterSet',
        100 => 'UniqueObjectName',
        120 => 'ARMIdentifier',
        122 => 'ARMVersion',
      ),
      2 => array( // IPTC ApplicationRecord Tags
        0 => 'ApplicationRecordVersion',
        3 => 'ObjectTypeReference',
        4 => 'ObjectAttributeReference',
        5 => 'ObjectName',
        7 => 'EditStatus',
        8 => 'EditorialUpdate',
        10 => 'Urgency',
        12 => 'SubjectReference',
        15 => 'Category',
        20 => 'SupplementalCategories',
        22 => 'FixtureIdentifier',
        25 => 'Keywords',
        26 => 'ContentLocationCode',
        27 => 'ContentLocationName',
        30 => 'ReleaseDate',
        35 => 'ReleaseTime',
        37 => 'ExpirationDate',
        38 => 'ExpirationTime',
        40 => 'SpecialInstructions',
        42 => 'ActionAdvised',
        45 => 'ReferenceService',
        47 => 'ReferenceDate',
        50 => 'ReferenceNumber',
        55 => 'DateCreated',
        60 => 'TimeCreated',
        62 => 'DigitalCreationDate',
        63 => 'DigitalCreationTime',
        65 => 'OriginatingProgram',
        70 => 'ProgramVersion',
        75 => 'ObjectCycle',
        80 => 'By-line',
        85 => 'By-lineTitle',
        90 => 'City',
        92 => 'Sub-location',
        95 => 'Province-State',
        100 => 'Country-PrimaryLocationCode',
        101 => 'Country-PrimaryLocationName',
        103 => 'OriginalTransmissionReference',
        105 => 'Headline',
        110 => 'Credit',
        115 => 'Source',
        116 => 'CopyrightNotice',
        118 => 'Contact',
        120 => 'Caption-Abstract',
        121 => 'LocalCaption',
        122 => 'Writer-Editor',
        125 => 'RasterizedCaption',
        130 => 'ImageType',
        131 => 'ImageOrientation',
        135 => 'LanguageIdentifier',
        150 => 'AudioType',
        151 => 'AudioSamplingRate',
        152 => 'AudioSamplingResolution',
        153 => 'AudioDuration',
        154 => 'AudioOutcue',
        184 => 'JobID',
        185 => 'MasterDocumentID',
        186 => 'ShortDocumentID',
        187 => 'UniqueDocumentID',
        188 => 'OwnerID',
        200 => 'ObjectPreviewFileFormat',
        201 => 'ObjectPreviewFileVersion',
        202 => 'ObjectPreviewData',
        221 => 'Prefs',
        225 => 'ClassifyState',
        228 => 'SimilarityIndex',
        230 => 'DocumentNotes',
        231 => 'DocumentHistory',
        232 => 'ExifCameraInfo',
      ),
      3 => array( // IPTC NewsPhoto Tags
        0 => 'NewsPhotoVersion',
        10 => 'IPTCPictureNumber',
        20 => 'IPTCImageWidth',
        30 => 'IPTCImageHeight',
        40 => 'IPTCPixelWidth',
        50 => 'IPTCPixelHeight',
        55 => 'SupplementalType',
        60 => 'ColorRepresentation',
        64 => 'InterchangeColorSpace',
        65 => 'ColorSequence',
        66 => 'ICC_Profile',
        70 => 'ColorCalibrationMatrix',
        80 => 'LookupTable',
        84 => 'NumIndexEntries',
        85 => 'ColorPalette',
        86 => 'IPTCBitsPerSample',
        90 => 'SampleStructure',
        100 => 'ScanningDirection',
        102 => 'IPTCImageRotation',
        110 => 'DataCompressionMethod',
        120 => 'QuantizationMethod',
        125 => 'EndPoints',
        130 => 'ExcursionTolerance',
        135 => 'BitsPerComponent',
        140 => 'MaximumDensityRange',
        145 => 'GammaCompensatedValue',
      ),
      7 => array( // IPTC PreObjectData Tags
        10 => 'SizeMode',
        20 => 'MaxSubfileSize',
        90 => 'ObjectSizeAnnounced',
        95 => 'MaximumObjectSize',
      ),
      8 => array( // IPTC ObjectData Tags
        10 => 'SubFile',
      ),
      9 => array( // IPTC PostObjectData Tags
        10 => 'ConfirmedObjectSize',
      ),
    );
  }
  return (isset($IPTC_record_tag_name[$IPTC_record][$IPTC_tag_key]) ? $IPTC_record_tag_name[$IPTC_record][$IPTC_tag_key] : $IPTC_tag_key);
}
